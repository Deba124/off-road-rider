﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Becker.MVC;

namespace RideHard{
	public class GameView : View<ApplicationGameManager> {

		/*ScoreManager scrManager;

		public ScoreManager ScrManager { get { return scrManager = Assert<ScoreManager> (scrManager); } }*/

		DistanceCalculator dstCalc;
		public DistanceCalculator DstCalc{ get { return dstCalc = Assert<DistanceCalculator> (dstCalc); } }

		Nitro _nitro;
		public Nitro nitro { get { return _nitro = Assert<Nitro> (_nitro); } }

		GetScore_StoreScore getScore_storeScore;
		public GetScore_StoreScore _getScore_storeScore { get {return getScore_storeScore = Assert<GetScore_StoreScore> (getScore_storeScore); } }
}
}