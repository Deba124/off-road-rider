﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Becker.MVC;

namespace RideHard{
	public class Nitro : View<ApplicationGameManager> {

		public bool nitroEnabled = true;
		public ParticleSystem[] NitroPrticle;
		float prevVelocity;
		public void StartNitro(){

			if (app.model.coinCollect.coinCounter >= 30) {
				
				app.model.coinCollect.coinCounter = 0;
				if (app.model.carSelect.SelectedCar == "Car1") {
					NitroPrticle[0].Play ();
					}
				if (app.model.carSelect.SelectedCar == "Car2") {
					NitroPrticle[1].Play ();
				}
				prevVelocity = app.model.carModel.rb.velocity.x;
				app.model.carModel.rb.velocity = new Vector3 (app.model.carModel.NitroSpeed, 0, 0);
			}
		}


		public void StopNitro(){

			app.model.coinCollect.Nitro.color = new Color (255, 255, 255, 0.3f);
			if (app.model.carModel.rb.velocity.x != 0f) {

				if (app.model.carSelect.SelectedCar == "Car1")
				NitroPrticle[0].Stop ();

				if (app.model.carSelect.SelectedCar == "Car2")
					NitroPrticle[1].Stop ();
			}
		}

}
}