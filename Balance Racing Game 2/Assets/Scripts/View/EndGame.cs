using UnityEngine;
using UnityEngine.SceneManagement;
using Becker.MVC;

namespace RideHard{
	public class EndGame : Controller<ApplicationGameManager> {

		void OnTriggerEnter2D (Collider2D colInfo)
	{
		if (colInfo.gameObject.tag.Equals("Car"))
		{
                Debug.Log("Car Turned");
                app.controller._gmOver.OverGame("Car Turned");
		}
	}

}
}
