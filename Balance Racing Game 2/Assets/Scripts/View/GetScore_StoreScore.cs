﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Becker.MVC;
using System;

namespace RideHard{
	public class GetScore_StoreScore : View<ApplicationGameManager>{
	
		string path;
        string pathForSafeKeyStorage;
		public int CollectedCoins;

		void Awake(){
            pathForSafeKeyStorage = Application.persistentDataPath+"/revsites/delimeter/convogister/zednx/127&&__.txt";
        path = Application.persistentDataPath + "/Score.txt";
		//File.Create (path).Close();
			WriteFirst ();

			Debug.Log (Application.persistentDataPath);
		}

		void WriteFirst (){
			if (!File.Exists (path)) {
				StreamWriter writer = new StreamWriter (path, true);
				writer.WriteLine ("0");
				writer.Close ();
				return;
			} else
				return;

		}

	/// <summary>
    /// This Function Actually writes collected coins into file when ever the game is over
        /// This function first clears the file which will store the coins, Then it calls NowWrite_File function to ultimately store the collected coins.
    /// </summary>
    /// <param name="NewCoins">New coins.</param>
		public void WriteIntoFile_Score(string NewCoins){
			File.Create (path).Close(); // This line actually clears the existing file, if it is not presented then it just creates it.
			NowWrite_File (NewCoins);

			}
        /// <summary>
        /// This function actually writes the collected coins into the file, which will be retrive when the user will open the game again.
        /// </summary>
        /// <param name="NewCoins">New coins.</param>
		void NowWrite_File(string NewCoins){
		
			StreamWriter writer = new StreamWriter(path, true);
			writer.WriteLine(NewCoins);
			writer.Close();
		}
	

        /// <summary>
        /// this function reads the coins from file, if it exists.
        /// </summary>
        /// <returns>The from file score.</returns>
		public int WroteFromFile_Score(){

			StreamReader reader = new StreamReader (path, true);
			CollectedCoins = Convert.ToInt32(reader.ReadLine ());
			Debug.Log (CollectedCoins);
			reader.Close ();
			return CollectedCoins;

		}

        public void StoreSafeKey(int numberOfKeysCollected){

            File.Create(pathForSafeKeyStorage).Close();

            NowWriteIntoLocalStorage(numberOfKeysCollected);
        }

        void NowWriteIntoLocalStorage(int numberOfKeysCollected){


        }
}
}