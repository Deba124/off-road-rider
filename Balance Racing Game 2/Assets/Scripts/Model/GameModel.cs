﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Becker.MVC;

namespace RideHard{
	public class GameModel : Model<ApplicationGameManager> {
		public Button GameStats;
		public InputField Coin, Nitro;
		//All Essential variables and class objects
        public GameObject 	CurTurnedGameOverModel;
		public GameObject 	PauseScreen;
        public GameObject   StarParticle;
		public int 			AllCoins;
		public Text 		TotalCoins, DesiredCoinsEngine, DesiredCoinsNitro;
		public GameObject 	Blast, StartRiding,CarToBeActivated,UpgradeButton,CoinSpawnner;
		public Animator 	CamJerk;
		public string 		LandSelection;
		public Animator 	AppearCarChoose;
		public GameObject 	CarToBuyPurchasePanel_1,CarToBuy_1, HackUtilitiesMenu, StarterScreen;
		[HideInInspector]
		public GameObject DevelopMenuRecentHolder;
		public GameObject DeveloperConsole,SysInfo, GraphicsInfo;
		//Field selection Components
		public GameObject DesertLand,GrassLand,MapToScroll_grassLand,MapToScroll_desrtLand;
        [HideInInspector]
        public int StarCollection, SafeKey;

		//only class references required to be instanced
		CarModelSpecfication CarModel;
		public CarModelSpecfication carModel { get { return CarModel = Assert<CarModelSpecfication> (CarModel); } }
		ScoreDetails scrDetails;
		public ScoreDetails ScrDetails { get { return scrDetails = Assert<ScoreDetails> (scrDetails); } }
		BackgroundScolling _backScrl;
		public BackgroundScolling backScrl { get { return _backScrl = Assert<BackgroundScolling> (_backScrl); } }
		CoinCollectModel _coinCollect;
		public CoinCollectModel coinCollect{ get { return _coinCollect = Assert<CoinCollectModel> (_coinCollect); } }
		carSelection _carSelect;
		public carSelection carSelect { get { return _carSelect = Assert<carSelection> (_carSelect); } }
		AudioModels audModels;
		public AudioModels _audModels { get { return audModels = Assert<AudioModels> (audModels); } }

}
}