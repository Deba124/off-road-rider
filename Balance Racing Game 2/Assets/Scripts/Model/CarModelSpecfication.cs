﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;
using Becker.MVC;

namespace RideHard{
	public class CarModelSpecfication : Model<ApplicationGameManager> {
		//properties for car
		public Button Right, Left;
		public GameObject Car1, Car2;
		[HideInInspector]
		public AxisTouchButton Rightaxis ,Leftaxis;
		[HideInInspector]
		public float 		speed = 1f; 
		[HideInInspector]
		public static float Counter, MainCount = 3f;
		[HideInInspector]
		public float 		rotationSpeed = 1f;
		public Transform 	coinSpawner;
		public WheelJoint2D[] backAndFrontWheel_car1 = new WheelJoint2D[2];
		public WheelJoint2D[] backAndFrontWheel_car2 = new WheelJoint2D[2];


		[HideInInspector]
		public float NitroSpeed,RockFallTime,BridgeFallTime;
		[HideInInspector]
		public GameObject CarRigid;
		[HideInInspector]
		public Rigidbody2D rb;
		[HideInInspector]
		public float movement = 0f;
		[HideInInspector]
		public float rotation = 0f;
		[HideInInspector]
		public float CarEnigineCapacity;

		//properties for camera
		public Transform[] CameraToFollow;
		public Camera cam;

		public void InitCarProperties(){

			switch (app.model.carSelect.SelectedCar) {

			case "Car1": 
				rb = Car1.GetComponent<Rigidbody2D> ();
				RockFallTime = .4f;
				BridgeFallTime = 1.9f;
				CarRigid = Car1;
				break;

			case "Car2":
				rb = Car2.GetComponent<Rigidbody2D> ();
				NitroSpeed = 30f;
				RockFallTime = .6f;
				BridgeFallTime = 1.9f;
				CarRigid = Car2;
				break;
				
			}
		}

		/// <summary>
		/// Actions from left button.
		/// </summary>
		/// <returns>The from left button.</returns>
//		public void ActionFromLeftButton(){
//			
//			StartCoroutine ("FrontGearUp");	
//		}
//		/// <summary>
//		/// Actions from right button.
//		/// </summary>
//		/// <returns>The from right button.</returns>
//		public void ActionFromRightButton(){
//
//			StartCoroutine ("BackGearUp");		
//
//		}
//		/// <summary>
//		/// Actions from left button up.
//		/// </summary>
//		/// <returns>The from left button up.</returns>
//		public void ActionFromLeftButton_Up(){
//			StopCoroutine ("FrontGearUp");
//			Counter = 0f;MainCount = 3f;
//		}
//		/// <summary>
//		/// Actions from right button up.
//		/// </summary>
//		/// <returns>The from right button up.</returns>
//		public void ActionFromRightButton_Up(){
//			StopCoroutine ("BackGearUp");
//			Counter = 0f;MainCount = 3f;
//		}
//
//
//		IEnumerator FrontGearUp(){
//
//			while (true) {
//			
//				if (Counter > MainCount) {
//				
//					Leftaxis.responseSpeed += 100f;
//
//					//Decreases BackGear
//					if (Rightaxis.responseSpeed > 100f)
//						Rightaxis.responseSpeed -= 100f;
//
//
//					MainCount += 3f;
//				}
//				Counter++;
//				yield return new WaitForSeconds (1f);
//			}
//		}
////		IEnumerator BackGearUp(){
////
////			while (true) {
////
////				if (Counter > MainCount) {
////
////					Rightaxis.responseSpeed += 100f;
////					MainCount += 3f;
////				}
////				Counter++;
////				yield return new WaitForSeconds (1f);
////			}
////		}


}
}