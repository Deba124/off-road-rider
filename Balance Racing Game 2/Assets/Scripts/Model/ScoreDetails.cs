﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Becker.MVC;

namespace RideHard{
	public class ScoreDetails : Model<ApplicationGameManager> {

		private int Score;
		public Text txt;

		public int score { get { return Score; } set { Score = value; } }
	
}
}
