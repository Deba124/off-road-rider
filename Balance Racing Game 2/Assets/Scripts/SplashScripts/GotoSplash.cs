﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Becker.MVC;

namespace RideHard{
	public class GotoSplash : Controller<ApplicationGameManager> {

	// Use this for initialization
	void Start () {
		StartCoroutine ("GoToSplash");
		}

		
	IEnumerator GoToSplash(){
		yield return new WaitForSeconds (2f);
		Initiate.Fade ("Splash", Color.black, 2.0f);
			StopCoroutine("GoToSplash");
	}
	}
}
