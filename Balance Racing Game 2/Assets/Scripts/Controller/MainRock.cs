﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Becker.MVC;

namespace RideHard{
	public class MainRock : Controller<ApplicationGameManager> {

		public Rigidbody2D[] smallRockRigid = new Rigidbody2D[2];

		void OnTriggerEnter2D(Collider2D col){
		
			if (col.gameObject.tag == "Car") {
				StartCoroutine ("FellTheRocks");
			}
		}

		IEnumerator FellTheRocks(){
			yield return new WaitForSeconds (.4f);
			smallRockRigid [0].bodyType = RigidbodyType2D.Dynamic;
			smallRockRigid [1].bodyType = RigidbodyType2D.Dynamic;
			StopCoroutine ("FellTheRocks");
		}
}
}
