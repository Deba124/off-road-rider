﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Becker.MVC;

namespace RideHard
{
    public class CoinFallDetector : Controller<ApplicationGameManager>
    {
        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.tag.Equals("CoinSpawnner"))
            {
                if(app.controller.CoinPosToResurrect != null)
                collision.gameObject.transform.position = new Vector3(app.controller.CoinPosToResurrect.transform.position.x + 36.0f, app.controller.CoinPosToResurrect.transform.position.y, app.controller.CoinPosToResurrect.transform.position.z);
            }
        }

    }
}