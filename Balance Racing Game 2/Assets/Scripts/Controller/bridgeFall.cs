﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Becker.MVC;

namespace RideHard{
	public class bridgeFall : Controller<ApplicationGameManager> {
		void OnTriggerEnter2D(Collider2D col){
			
			if(col.gameObject.tag.Equals("Car"))
			StartCoroutine ("FallBridge");
		}

		IEnumerator FallBridge(){
		
			yield return new WaitForSeconds (app.model.carModel.BridgeFallTime);

			app.controller.bridgeSpawn.Holdthis.GetComponent<Rigidbody2D> ().bodyType = RigidbodyType2D.Dynamic;

			StopCoroutine ("FallBridge");
		}

		void OnBecameInvisible(){

			Destroy (app.controller.bridgeSpawn.Holdthis);

		}
}
}