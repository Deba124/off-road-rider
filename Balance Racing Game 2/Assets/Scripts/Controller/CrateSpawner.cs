﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Becker.MVC;

namespace RideHard{
	public class CrateSpawner : Controller<ApplicationGameManager> {

		public Transform[] Spawnarea = new Transform[5];
		public GameObject crate;
		int range;
		bool Playing = true;

		void Start(){
			StartCoroutine ("SpawnCrate");
		}
		/// <summary>
		/// Spawns the crate.
		/// </summary>
		/// <returns>The crate.</returns>
	public IEnumerator SpawnCrate(){
		
			while (Playing) {
				range = Random.Range (0, 4);

				GameObject crateFalled = Instantiate (crate, Spawnarea [range].position, Spawnarea [range].rotation);
				yield return new WaitForSeconds (3f);

				Destroy (crateFalled, 1f);
			}
		}

}
				}
