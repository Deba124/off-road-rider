﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Becker.MVC;

namespace RideHard{
	public class SpeedMeter : Controller<ApplicationGameManager> {

		static float MinAngle=159f;
		static float MaxAngle=-137f;
		//public Image meterCompass;

		static SpeedMeter _spdmeter;

		void Start(){
		
			_spdmeter = this;
		}

		//[Range(0,100)]
		//public float meterSpeed;

		public static void CheckSpeed(float speed, float min, float max){
		

				
			float ang = Mathf.Lerp (MinAngle, MaxAngle, Mathf.InverseLerp (min, max, speed*2.99f));
			_spdmeter.transform.eulerAngles = new Vector3 (0, 0, ang);
			//Debug.Log (_spdmeter.transform.eulerAngles.z);

				
			}
				
		}


	
}
