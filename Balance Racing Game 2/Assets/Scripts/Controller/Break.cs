﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Becker.MVC;

namespace RideHard{
	public class Break : Controller<ApplicationGameManager> {

		public void CarBreak(){

			app.model.carModel.rb.velocity = new Vector3 (0f, 0f, 0f);
		}
}
}