﻿
using UnityEngine;
using Becker.MVC;

namespace RideHard
{
    public class IfStartCollected : Controller<ApplicationGameManager>
    {

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if( collision.gameObject.tag.Equals("Car")){

                Instantiate(app.model.StarParticle, this.gameObject.transform.position, this.gameObject.transform.rotation);
                app.model.StarCollection++;
                Destroy(this.gameObject);
            }
        }

    }
}
