﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Becker.MVC;

namespace RideHard{
	public class CoinCreator : Controller<ApplicationGameManager> {

		public GameObject coin,ParentObject,holdCoinSpawn,JerryCan,Star;
		[HideInInspector]
		public float PrevCoinPos = 0f;
        [HideInInspector]
        GameObject HoldStarSpawn;
		float counter;


		void Start(){
            InvokeRepeating("StarSpawnner", 8f, 8f);

		}

		void Update(){
			if(app.controller.InPlayMode){
			if(app.model.carModel.rb.velocity.x > 2f)
			counter += Time.deltaTime;
              //  Debug.Log("The Counter right now: " + counter);
		}
		}

        public void StarSpawnner()
        {
            HoldStarSpawn = Instantiate(Star);
            HoldStarSpawn.transform.position = new Vector3(transform.position.x, transform.position.y, -0.59f);
            Debug.Log("Star Spawnned");
        }

		/// <summary>
		/// Spwans the coin.
		/// </summary>
		/// <returns>The coin.</returns>
		public IEnumerator SpwanCoin(){
			
			while (true) {

				if (app.controller.InPlayMode) {

					if (app.model.carModel.rb.velocity.x > 2f && transform.position.x - PrevCoinPos > 2f) {
						PrevCoinPos = transform.position.x;
						if (counter >15f) {
							holdCoinSpawn = Instantiate (JerryCan);
							counter = 0f;
							Debug.Log ("Jerrycan Spawned");
						}
                        
                        else {
							holdCoinSpawn = Instantiate (coin);
							//Debug.Log ("CoinSpawned");
						}
				
						//Debug.Log ("The Velocity is: " + app.model.carModel.rb.velocity.x);

						//holdCoinSpawn.transform.parent = ParentObject.transform;
						holdCoinSpawn.transform.position = new Vector3 (transform.position.x, transform.position.y, -0.59f);
					}	
				}
					//Debug.Log ("TheTimeIs: "+counter);
					yield return new WaitForSeconds (.5f);
				
			}

		}


}
}