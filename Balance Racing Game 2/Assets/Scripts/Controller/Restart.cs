﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Becker.MVC;

namespace RideHard{
public class Restart : MonoBehaviour {

		public void RestartGame(){
		
			Initiate.Fade ("MainLevel", Color.black, 1.0f);
		}

		public void ReturnToMainMenu(){
		
			Initiate.Fade ("Splash", Color.black, 2.0f);
		}
}
}