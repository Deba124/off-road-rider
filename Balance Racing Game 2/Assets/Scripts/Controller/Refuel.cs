﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Becker.MVC;

namespace RideHard{
	public class Refuel : Controller<ApplicationGameManager> {

		void OnTriggerEnter2D(Collider2D col){
		
			if(col.gameObject.tag == "Car"){
				app.controller.fuelControl.StopCoroutine ("FuelControl");
				app.controller.fuelControl.StopCoroutine ("FuelTxtControl");

				app.controller.fuelControl.StartCoroutine ("ReFuel");
				app.controller.fuelControl.StartCoroutine ("RefuelTXT");
				Destroy (this.transform.parent.gameObject);


			}
		}
}
}