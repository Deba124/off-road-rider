﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using Becker.MVC;
using System;

namespace RideHard{
	public class ChooseCar : Controller<ApplicationGameManager>{

		public Animator SlideDownAnimAfterChoose;
        private GameObject SelectionMark;
		string path, TotalDetails;
		string[] CarInfos;
		public void ChooseCar1(GameObject CarForNow)
			{
            if (SelectionMark != null)
                SelectionMark.SetActive(false);
            SelectionMark = null;
            ResetCarDataFirst ();
			ResetUpgradePanel ();
			EnableUpgradeButton ();
			GetInfo ("Car1");
			app.model.carSelect.SelectedCar = "Car1";
			app.model.StartRiding.SetActive (true);
			app.model.CarToBeActivated = app.model.carModel.Car1;
            SelectionMark = CarForNow.GetComponent<RectTransform>().GetChild(0).gameObject;
            SelectionMark.SetActive(true);
            //Debug.Log ("Car To Be Activated - Next: " + app.model.CarToBeActivated);
            //ActivateNow ();
        }

		public void ChooseCar2(GameObject CarForNow)
		{
            if (SelectionMark != null)
                SelectionMark.SetActive(false);
            SelectionMark = null;
            ResetCarDataFirst ();
			ResetUpgradePanel ();
			EnableUpgradeButton ();
			GetInfo ("Car2");
			app.model.carSelect.SelectedCar = "Car2";
			app.model.StartRiding.SetActive (true);
			app.model.CarToBeActivated = app.model.carModel.Car2;
            SelectionMark = CarForNow.GetComponent<RectTransform>().GetChild(0).gameObject;
            SelectionMark.SetActive(true);

            }

		void ResetUpgradePanel()
		{
			app.controller._upgrade.UpgradePanel.transform.localScale = new Vector3 (0, 0, 1);
			app.controller._upgrade.UpgradeAnim.SetBool ("Upgrade", false);
		}

		void EnableUpgradeButton()
		{
			app.model.UpgradeButton.GetComponent<Image> ().color 	= new Color (255f, 255f, 255f, 1.0f);
			app.model.UpgradeButton.GetComponent<Button> ().enabled = true;
            app.model.UpgradeButton.GetComponent<RectTransform>().GetChild(0).gameObject.SetActive(true);

        }

        /// <summary>
        /// Activates Choosen Car in the Scene.
        /// </summary>
        /// <returns>The now.</returns>
        public void ActivateNow()
		{   Debug.Log ("Car To Be Activated - Next: " + app.model.CarToBeActivated);
			app.model.CarToBeActivated.SetActive (true);
			SlideDownAnimAfterChoose.SetBool ("Choosen", true);
			app.controller.CarChoosed = true;
		}

		void ResetCarDataFirst()
		{
			if (app.model.CarToBeActivated != null) {
				app.model.CarToBeActivated = null;
				Debug.Log ("Car To Be Activated: " + app.model.CarToBeActivated);
			}
			return;
		}

		void GetInfo(string SelectedCar)
		{	
			path = Application.persistentDataPath + "/" + SelectedCar + ".txt";
			if (File.Exists (path)) {
				StreamReader reader = new StreamReader (path, true);
				TotalDetails = reader.ReadToEnd ();
				CarInfos = TotalDetails.Split (' ');
				app.model.carModel.CarEnigineCapacity = (float)Convert.ToDouble(CarInfos[0]);
				app.model.carModel.NitroSpeed = (float)Convert.ToDouble(CarInfos[1]);
				Debug.Log ("The Capacity" + app.model.carModel.CarEnigineCapacity);
				Debug.Log ("The Nitro" + app.model.carModel.NitroSpeed);
				reader.Close ();
			} else {
				SetDataInFileOfCarSpecs ("200","30",path);
				app.model.carModel.NitroSpeed = 30f;
				app.model.carModel.CarEnigineCapacity = 200f;
				}
			}
		public void SetDataInFileOfCarSpecs(string CarEnineCapacity, string NitroSpeed, string FileToFind)
		{
			StreamWriter writer = new StreamWriter (FileToFind, true);
			writer.WriteLine (CarEnineCapacity + " " +NitroSpeed);
			writer.Close ();
		}
		public void ResetFileData(string CarFilePath)
		{
			File.Create (CarFilePath).Close();
		}
}
}