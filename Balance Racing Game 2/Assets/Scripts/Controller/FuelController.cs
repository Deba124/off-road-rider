﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using Becker.MVC;

namespace RideHard{
	public class FuelController : Controller<ApplicationGameManager> {

		public Slider FuelSlider;
		public Text FuelText;
		public float sliderValue = 1f;
		public int sliderValueToShow = 30;

		void Start(){
			

		}

		public IEnumerator FuelControl(){
		
			while (sliderValue > 0f) {
				yield return new WaitForSeconds (.0125f);
				if (app.controller.InPlayMode && !app.controller.IsPaused) { 
					

					sliderValue -= 0.0006f;
					//Debug.Log ("The Slider Value is:"+ sliderValue);
					FuelSlider.value = sliderValue;


				}
			}

		}
		public IEnumerator FuelTxtControl(){
			
			while (sliderValueToShow > 0) {
				yield return new WaitForSeconds (1f);
				if (app.controller.InPlayMode && !app.controller.IsPaused) {
					
					sliderValueToShow--;
					FuelText.text = sliderValueToShow.ToString ();
					if (sliderValueToShow == 0) {
						app.controller._gmOver.OverGame ("FuelOver");
						break;
					}
					
				}
			}



		}

		public IEnumerator ReFuel(){

			while (sliderValue < 1f) {

				yield return null;
				if (app.controller.InPlayMode && !app.controller.IsPaused) {
					

					sliderValue += 0.01f;
					//Debug.Log ("The Slider Value is:"+ sliderValue);
					FuelSlider.value = sliderValue;


				}
			}
			//app.controller.Refueld = true;

}
		public IEnumerator RefuelTXT(){
			

				while (sliderValueToShow < 30) {
					yield return new WaitForSeconds (.1f);
					sliderValueToShow++;
					FuelText.text = sliderValueToShow.ToString ();
					}
			
				app.controller.Refueld = true;

			}
		}
}