﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Becker.MVC;

namespace RideHard{
	public class CoinCollect : Controller<ApplicationGameManager> {

		public Animation Collect;

		void Start(){
			Collect = GetComponent<Animation> ();
//			Collect ["CollectCoin"].speed = 1f;
		}

		void OnTriggerEnter2D(Collider2D col){
			if (col.gameObject.tag == "Car") {
				app.model.coinCollect.coinCounter++;

				if (app.model.coinCollect.coinCounter == 30)
					app.model.coinCollect.Nitro.color = new Color(255,255,255,255);
				if(app.model._audModels.PlayAudioAll)
					app.model._audModels.coinAudio.Play ();

				Collect.Play (Collect.clip.name);
				StartCoroutine ("CountCoin");


			}
		}

		IEnumerator CountCoin(){

			yield return new WaitForSeconds (0.5f);
			app.model.coinCollect.Count++;
			app.model.coinCollect.CoinCounter.text =app.model.coinCollect. Count.ToString ();
			Destroy (this.transform.parent.gameObject);
			Collect.Stop(Collect.clip.name);
			StopCoroutine ("CountCoin");


		}

		void OnBecameInvisible()
		{
			Destroy (this.gameObject);
		}
}
}
