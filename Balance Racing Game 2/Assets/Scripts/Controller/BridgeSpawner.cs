﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Becker.MVC;

namespace RideHard{
	public class BridgeSpawner : Controller<ApplicationGameManager> {

		public GameObject Holdthis;
	public void SpawnBridge(){

			if (app.controller.InPlayMode && !app.controller.IsPaused) {
				if (app.controller.ObsSpwn.notSpawned) {
					int i = Random.Range (0, 2);
					Holdthis = Instantiate (app.controller.ObsSpwn.Bridge [i]);
					Holdthis.transform.parent = app.controller.ObsSpwn.MapMain3.transform;

					if (Holdthis.tag.Equals ("Bridge2"))
						Holdthis.transform.position = new Vector3 (transform.position.x, 1.85f, -0.59f);
					else
						Holdthis.transform.position = new Vector3 (transform.position.x, 2.62f, -0.59f);

					app.controller.ObsSpwn.notSpawned = false;
					Debug.Log ("Bridge Spawned");
				}
			}
		}
}		
}
